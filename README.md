# Training: Overview in AI

This Git directory is provided as part of the "Overview in AI" training on 25 May 2023.

## Notebooks

- [Multi-layer perceptron and CNN](mnist_mlp_cnn.ipynb)
- [Fine-tuning with Visual Transformer from HuggingFace](mnist_finetuning.ipynb)
- [Python AI tools](python_tools.ipynb)

## Slides

- [AI Overview slides](Tour_Horizon_IA_25mai2023.pdf)

## Contact

For questions, comments, please contact pnria-sed-rennes@inria.fr

## Authors

- BETTON Thomas
- COURTEILLE Hermann
- LEROUX Cyrille
